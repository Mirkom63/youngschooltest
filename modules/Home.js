var lang_home={
  'en' : {
    'title' : "Begin test and learn what career is right for you!",
    'button': 'Begin test',
    'page' : 'Test',
  },
  'ru' : {
    'title' : "Пройди тест и узнай кем ты можешь стать!",
    'button' : 'Пройти тест',
    'page' : 'Тест',
  }
}

class Home extends React.Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerShown: false,
    };
  };


  render() {

    var width = Dimensions.get('window').width*0.8;
    return (

      <View style={{flex: 1, alignItems: 'center'}}>
        <ImageBackground
          style={{flex: 1, justifyContent: 'center', width: '100%'}}
          source={require('./images/background.png')}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>


            <Text
              style={{
                fontSize: 28,
                color: '#000',
                textAlign: 'center',
                padding: 30,
              }}>
              {lang_home[root_app.state.language]['title']}
            </Text>

            <Image
              source={require('./images/home_image.png')}
              style={{width: '80%', height: width}}
            />

            <TouchableOpacity style={{width: 200, height: 40, backgroundColor: '#407de7', borderRadius: 5, marginTop: 20}} onPress={()=>this.props.navigation.navigate(lang_home[root_app.state.language]['page'])}>
              <Text style={{color: '#FFF', textAlign: 'center', lineHeight: 40, fontSize: 20}}>
                {lang_home[root_app.state.language]['button']}
              </Text>
            </TouchableOpacity>

            <View style={{height: 60, width: '100%'}}>
              <View style={{flex: 1, justifyContent: 'center', width: '100%', height: 60, flexDirection: 'row', marginTop: 20}}>
                <TouchableOpacity onPress={()=>root_app.set_language('en')} style={{height: 60, marginRight: 20}}>

                  <View style={{flex: 1, flexDirection: 'row', height: 100, justifyContent: 'center'}}>
                    <View style={{width: 40, height: 40, marginTop: 10, borderRadius: 20, overflow: 'hidden', borderColor: '#fff', borderWidth: 3,}}>
                      <Image
                        style={{width: 36, height: 36}}
                        source={require('./images/lang_en.jpg')}
                      />
                    </View>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>root_app.set_language('ru')} style={{height: 60}}>
                  <View style={{flex: 1, flexDirection: 'row', height: 100, justifyContent: 'center'}}>
                    <View style={{width: 40, height: 40, marginTop: 10, borderRadius: 20, overflow: 'hidden', borderColor: '#fff', borderWidth: 3,}}>
                      <Image
                        style={{width: 36, height: 36}}
                        source={require('./images/lang_ru.jpg')}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>



          </View>
        </ImageBackground>
      </View>
    );
  }
}
