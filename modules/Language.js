class Language extends React.Component {
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>

          <View style={{height: 100}}>
            <Text style={{textAlign: 'center', marginBottom: 10, fontSize: 20}}>Select language</Text>
            <Text style={{textAlign: 'center', marginBottom: 30, fontSize: 20}}>Выберите язык</Text>
          </View>


          <View>
            <TouchableOpacity onPress={()=>root_app.set_language('en')} style={{height: 60, borderColor: '#ddd', borderBottomWidth: 1, borderTopWidth: 1}}>

              <View style={{flex: 1, flexDirection: 'row', height: 100, justifyContent: 'center'}}>
                <View style={{width: 40, height: 40, marginTop: 10, borderRadius: 20, overflow: 'hidden', borderColor: '#ddd', borderWidth: 3,}}>
                  <Image
                    style={{width: 36, height: 36}}
                    source={require('./images/lang_en.jpg')}
                  />
                </View>
                <View style={{width: 150, height: 40, marginLeft: 10, marginTop: 10,}}>
                  <Text style={{lineHeight: 40, fontSize: 20}}>
                    English
                  </Text>
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>root_app.set_language('ru')} style={{height: 60, borderBottomColor: '#ddd', borderBottomWidth: 1}}>
              <View style={{flex: 1, flexDirection: 'row', height: 100, justifyContent: 'center'}}>
                <View style={{width: 40, height: 40, marginTop: 10, borderRadius: 20, overflow: 'hidden', borderColor: '#ddd', borderWidth: 3,}}>
                  <Image
                    style={{width: 36, height: 36}}
                    source={require('./images/lang_ru.jpg')}
                  />
                </View>
                <View style={{width: 150, height: 40, marginLeft: 10, marginTop: 10,}}>
                  <Text style={{lineHeight: 40, fontSize: 20}}>
                    Русский
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>

      </View>
    );
  }
}
