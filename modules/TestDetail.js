var lang_test={
  'en' : {
    'center_title' : "Choose the option that describes you best:",
    'back' : 'Back',
    'restart' : 'Restart',
    'share' : 'Share',
    'auth_info' : 'To get the results sign in with:',
  },
  'ru' : {
    'center_title' : "Выбери то, что подходит тебе больше всего",
    'back' : 'Назад',
    'restart' : 'Начать заново',
    'share' : 'Поделиться',
    'auth_info' : 'Чтобы посмотреть свои результаты, авторизуйтесь:',
  }
}

var test_root_app;
class TestDetail extends React.Component {
  constructor(props) {
    super(props);
    test_root_app = this;

    this.state = {
      page: 1,
      finish: false,
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({page: 1, auth: false});
  }



  static navigationOptions = ({ navigation }) => {

    const { params } = navigation.state;

    var button_back=<Button onPress={() =>  test_root_app.prevTest() } title={lang_test[root_app.state.language]['back']} />;
    var button_null=null;
    var button_finish=<Button onPress={() => test_root_app.restartTest() } title={lang_test[root_app.state.language]['restart']} />
    var button_share=<Button onPress={() => root_app.onShare(result_number) } title={lang_test[root_app.state.language]['share']} />

    return {
      headerLeft: () => (
        params && ( params.page<21 ? ( params.page>1 && button_back ) : ( params.page<21 ? button_null :  button_finish   ) )
      ),
      title: params && params.page<21 && params.page+'/'+data_test.length,
      headerRight: () => (
        params && params.page==21 ? ( params.auth==true && button_share) : button_finish
      ),
    };


  };


  save_result(){

    this.props.navigation.setParams({auth: true});

    axios.get(host+'/save_answer.php',{params:
      {
        user_id: root_app.state.user_id,
        answer_id: result_number,
        answer_title: ar_result[result_number]['name'],
      }
    }).then(function (response) {

      if(list_root_app!=undefined){
        list_root_app.updateResult();
      }

    });
  }

  prevTest() {
    this.setState(
      {
        page: this.state.page - 1,
      },
      function () {
        this.props.navigation.setParams({page: this.state.page});
      },
    );
  }

  nextTest(result) {
    data_test[this.state.page - 1]['result'] = result;

    if (this.state.page + 1 == 21){
      var otvet_1 = 0;
      var otvet_2 = 0;
      var otvet_3 = 0;
      var otvet_4 = 0;
      var otvet_5 = 0;

      var ar_otvet = [
        [1, 2, 0, 0, 0],
        [0, 0, 1, 2, 0],
        [2, 0, 0, 0, 1],
        [0, 1, 2, 0, 0],
        [0, 0, 0, 1, 2],
        [1, 0, 2, 0, 0],
        [0, 2, 0, 0, 1],
        [0, 0, 1, 0, 2],
        [0, 1, 0, 2, 0],
        [1, 0, 0, 2, 0],
        [1, 2, 0, 0, 0],
        [0, 0, 1, 2, 0],
        [2, 0, 0, 0, 1],
        [0, 1, 2, 0, 0],
        [0, 0, 0, 1, 2],
        [1, 0, 2, 0, 0],
        [0, 2, 0, 0, 1],
        [0, 0, 1, 0, 2],
        [0, 1, 0, 2, 0],
        [1, 0, 0, 2, 0],
      ];
      data_test.forEach(function (test, index) {
        otvet = ar_otvet[index];
        if (test.result==otvet[0]){
          otvet_1++;
        }
        if (test.result==otvet[1]){
          otvet_2++;
        }
        if (test.result==otvet[2]){
          otvet_3++;
        }
        if (test.result==otvet[3]){
          otvet_4++;
        }
        if (test.result==otvet[4]){
          otvet_5++;
        }
      });

      var r = [otvet_1,otvet_2,otvet_3,otvet_4,otvet_5].sort().reverse();

      if (r[0] == otvet_1){
        result_number = 0;
      }
      if (r[0] == otvet_2){
        result_number = 1;
      }
      if (r[0] == otvet_3){
        result_number = 2;
      }
      if (r[0] == otvet_4){
        result_number = 3;
      }
      if (r[0] == otvet_5){
        result_number = 4;
      }

      if(root_app.state.auth==true){
        test_root_app.save_result();
      }

    }

    if(this.state.page + 1 == 21){
      var finish=true;
    }else{
      var finish=false;
    }

    this.setState(
      {
        page: this.state.page + 1,
        finish: finish,
      },
      function () {
        this.props.navigation.setParams({page: this.state.page});
      },
    );
  }

  restartTest() {
    this.setState(
      {
        page: 1,
        finish: false,
      },
      function () {
        this.props.navigation.setParams({page: 1});
        this.props.navigation.popToTop();
      },
    );
  }


  render() {

    if (this.state.finish == true){

      if (root_app.state.auth == true){

        var width = Dimensions.get('window').width;

        return (
          <SafeAreaView style={{flex: 1}}>
            <ScrollView>
              <View style={{backgroundColor: '#FFF'}}>
                <View>
                  <Text
                    style={{fontSize: 30, textAlign: 'center', padding: 30}}>
                    {ar_result[result_number]['name']}
                  </Text>
                  <Image
                    source={ar_result[result_number]['image']}
                    style={{width: '100%', height: width}}
                  />
                  <HTMLView
                    style={{padding: 30}}
                    value={ar_result[result_number].detail}
                  />
                </View>
              </View>
            </ScrollView>
          </SafeAreaView>
        );
      } else {

        return (



          <View style={{padding: 30, flex: 1, flexDirection: 'column', justifyContent: 'center'}}>

            <View style={{flex: 1,justifyContent: 'center', alignItems: 'center',}}>

              <Text style={{textAlign: 'center', marginBottom: 30, fontSize: 20}}>{lang_test[root_app.state.language]['auth_info']}</Text>

              { root_app.state.language=='ru' &&
                <TouchableOpacity onPress={root_app.handleVKLogin}>
                  <Image
                    style={{width: 300, height: 57}}
                    source={require('./images/vk_ru.png')}
                  />
                </TouchableOpacity>
              }
              <TouchableOpacity onPress={root_app.handleFBLogin}>
                { root_app.state.language=='ru' &&
                  <Image
                    style={{width: 300, height: 57, marginTop: 30}}
                    source={require('./images/fb_ru.png')}
                  />
                }
                { root_app.state.language=='en' &&
                  <Image
                    style={{width: 300, height: 57, marginTop: 30}}
                    source={require('./images/fb_en.png')}
                  />
                }
              </TouchableOpacity>

              <AppleButton
                buttonStyle={AppleButton.Style.BLACK}
                buttonType={AppleButton.Type.SIGN_IN}
                style={{width: 300, height: 57, marginTop: 30}}
                onPress={() => root_app.onAppleButtonPress()}
              />

            </View>


          </View>

        );
      }
    } else {
      return (
        <View style={{flex: 1, backgroundColor: '#FFF'}}>

          <View
            style={{
              backgroundColor: '#407de7',
              width: '100%',
              height: 30,
              position: 'absolute',
              flex: 1,
            }}>
            <Text
              style={{color: '#FFF', textAlign: 'center', lineHeight: 30}}>
              {lang_test[root_app.state.language]['center_title']}
            </Text>
          </View>

          <View style={{flex: 1}}>

            <TouchableOpacity
              onPress={() => this.nextTest(1)}
              style={{flex: 1, justifyContent: 'center'}}>
              <Text style={{fontSize: 20, padding: 30, textAlign: 'center'}}>
                {data_test[this.state.page - 1]['answer_1']}
              </Text>
            </TouchableOpacity>



          </View>

          <View style={{flex: 1}}>

            <TouchableOpacity
              onPress={() => this.nextTest(2)}
              style={{flex: 1, justifyContent: 'center', borderTopColor: '#407de7', borderTopWidth: 2}}>
              <Text style={{fontSize: 20, padding: 30, textAlign: 'center'}}>
                {data_test[this.state.page - 1]['answer_2']}
              </Text>
            </TouchableOpacity>

          </View>
        </View>
      );
    }


  }
}





var result_number=1;

var data_test=[];

var data_test_ru = [
  {
    answer_1: 'Ухаживать за животными',
    answer_2: 'Обслуживать машины, приборы (следить, регулировать)',
    result: 1,
  },
  {
    answer_1: 'Помогать больным',
    answer_2: 'Составлять таблицы, схемы, программы для компьютеров',
    result: 1,
  },
  {
    answer_1:
      'Следить за качеством  иллюстраций (в книгах, журналах)',
    answer_2: 'Следить за состоянием, развитием растений',
    result: 1,
  },
  {
    answer_1:
      'Обрабатывать материалы (дерево, ткань, металл, пластмассу и т.п.)',
    answer_2: 'Доводить Товары до потребителя, рекламировать, продавать',
    result: 1,
  },
  {
    answer_1: 'Обсуждать научно-популярные книги, статьи',
    answer_2: 'Обсуждать художественные книги (или пьесы, концерты)',
    result: 1,
  },
  {
    answer_1: 'Выращивать животных какой-либо породы',
    answer_2:
      'Тренировать товарищей (или младших) в выполнении каких-либо действий (трудовых, учебных, спортивных)',
    result: 1,
  },
  {
    answer_1:
      'Копировать рисунки, изображения (или настраивать музыкальные инструменты)',
    answer_2:
      'Управлять каким-либо грузовым (подъемным или транспортным) средством – подъемным краном, трактором, тепловозом и др.',
    result: 1,
  },
  {
    answer_1:
      'Предоставлять, разъяснять людям нужные сведения (в справочных, call-центрах, на экскурсиях и т.д.)',
    answer_2:
      'Оформлять выставки, витрины (или участвовать в подготовке пьес, концертов)',
    result: 1,
  },
  {
    answer_1: 'Ремонтировать вещи, изделия (одежду, технику), жилище',
    answer_2: 'Искать и исправлять ошибки в текстах, таблицах, рисунках',
    result: 1,
  },
  {
    answer_1: 'Лечить животных',
    answer_2: 'Выполнять вычисления, расчеты',
    result: 1,
  },
  {
    answer_1: 'Выводить новые сорта растений',
    answer_2:
      'Конструировать, проектировать новые виды промышленных изделий (машины, одежду, дома, продукты питания и т.п.)',
    result: 1,
  },
  {
    answer_1:
      'Разрешать споры, ссоры между людьми, убеждать, разъяснять информацию.',
    answer_2:
      'Разбираться в чертежах, схемах, таблицах (проверять, уточнять, приводить в порядок)',
    result: 1,
  },
  {
    answer_1:
      'Организовывать работу кружков художественной самодеятельности.',
    answer_2: 'Изучать мир микробов.',
    result: 1,
  },
  {
    answer_1: 'Обслуживать, налаживать медицинские приборы, аппараты',
    answer_2:
      'Оказывать людям медицинскую помощь при ранениях, ушибах, ожогах и т.п.',
    result: 1,
  },
  {
    answer_1:
      'Составлять точные описания-отчеты о наблюдаемых явлениях, событиях, измеряемых объектах и др.',
    answer_2:
      'Художественно описывать, изображать события (наблюдаемые и представляемые)',
    result: 1,
  },
  {
    answer_1: 'Проводить лабораторные анализы в больнице',
    answer_2:
      'Принимать, осматривать больных, беседовать с ними, назначать лечение',
    result: 1,
  },
  {
    answer_1: 'Красить или расписывать стены помещений, поверхность изделий',
    answer_2: ' Осуществлять монтаж или сборку машин, приборов',
    result: 1,
  },
  {
    answer_1:
      'Организовать мероприятия для сверстников или младших (походы в театры, музеи, на экскурсии, в туристические походы и т.п.)',
    answer_2: 'Играть на сцене, принимать участие в концертах',
    result: 1,
  },
  {
    answer_1:
      'Изготовлять по чертежам детали, изделия (машины, одежду), строить здания',
    answer_2: 'Заниматься черчением, копировать чертежи, карты',
    result: 1,
  },
  {
    answer_1: ' Вести борьбу с болезнями растений, с вредителями леса, сада',
    answer_2:
      'Заниматься набором текстов (печатать).',
    result: 1,
  },
];


var data_test_en = [
  {
    answer_1: 'Take care of the animals',
    answer_2: 'Work with the technology, machines, tools',
    result: 1,
  },
  {
    answer_1: 'Help sick people or people that need support',
    answer_2: 'Organize, create tables, budgets, outlines, work on projects',
    result: 1,
  },
  {
    answer_1: 'Make sure that illustrations and visuals used the projects address it’s theme',
    answer_2: 'Observe wildlife, take care of plants, or do gardening',
    result: 1,
  },
  {
    answer_1: 'Work with materials (wood, textiles, metall, plastic etc)',
    answer_2: 'Promote the products or ideas to people',
    result: 1,
  },
  {
    answer_1: 'Discuss the scientific articles and books',
    answer_2: 'Discuss the fiction books, broadway shows, theater performances or music concerts',
    result: 1,
  },
  {
    answer_1: 'Take care after the pet',
    answer_2: 'Teach and train other people',
    result: 1,
  },
  {
    answer_1: 'Copy the images, illustrations or tune the music instruments',
    answer_2: 'Operate the forklift, the crane or heavy machinery',
    result: 1,
  },
  {
    answer_1: 'Explaine something to people and educate them',
    answer_2: 'Decorate the stage, windows, exhibitions or participate in the theater production, organize concerts, events etc.',
    result: 1,
  },
  {
    answer_1: 'Fix the things, clothing, equipment, cars, do remodelling or build something',
    answer_2: 'Fix the errors and correct papers and assignments',
    result: 1,
  },
  {
    answer_1: 'Treat animals',
    answer_2: 'Do calculations or planning',
    result: 1,
  },
  {
    answer_1: 'Research and develop new plant varieties and uses',
    answer_2: 'Design new products, cars, clothing, homes etc.',
    result: 1,
  },
  {
    answer_1:
      'Be a mediator in a conflict, teach, motivate, manage people',
    answer_2:
      'Work with the documents, schemes, plans, budgets, tables (organize, update them etc.)',
    result: 1,
  },
  {
    answer_1:
      'Oversee the group activities',
    answer_2: 'Explore the connections between microbial life, history of the earth and our dependence on micro-organisms',
    result: 1,
  },
  {
    answer_1: 'Service medical equipment',
    answer_2:
      'Provide first aid to people during an emergency, help an ill or injured person',
    result: 1,
  },
  {
    answer_1:
      'Do reports, outlines of the events, objects, concepts and stat the facts',
    answer_2:
      'Creative write and write essays describing the events or people that affected or interest me',
    result: 1,
  },
  {
    answer_1: 'Do labs',
    answer_2:
      'Survey people or check on people’s health and writing the prescriptions for their needs',
    result: 1,
  },
  {
    answer_1: 'Paint or decorate the walls, the surfaces',
    answer_2: 'Take apart, fix, and reassemble various equipment and machines (like a radio, watch, or washing machine)',
    result: 1,
  },
  {
    answer_1:
      'Organize: concerts, group gatherings, trips, tours to museums, excursions, camping etc.',
    answer_2: 'Act on the stage, perform, public speaking and/or lead the performances or activities',
    result: 1,
  },
  {
    answer_1:
      'Build the parts, things (cars, clothing, etc.), construct buildings according to the drawings',
    answer_2: 'Design, draw schemes and maps etc.',
    result: 1,
  },
  {
    answer_1: 'Explore the wildlife or work outside',
    answer_2:
      'Work on the computer',
    result: 1,
  },
];

var ar_result;

var ar_result_ru = [
  {
    name: 'Человек-природа',
    detail:
      '<p>Если вы любите работать в саду, огороде, ухаживать за растениями, животными, любите предмет биологию, то ознакомьтесь с профессиями типа «человек-природа».&nbsp;</p><p>Предметом труда для представителей большинства профессий типа «человек природа» являются:&nbsp;</p><p>• животные, условия их роста, жизни;&nbsp;</p><p>• растения, условия их произрастания.&nbsp;</p><p>Специалистам в этой области приходится выполнять следующие виды деятельности:&nbsp;</p><p>• изучать, исследовать, анализировать состояние, условия жизни растений или животных (агроном, микробиолог, зоотехник, гидробиолог, агрохимик, фитопатолог);&nbsp;</p><p>• выращивать растения, ухаживать за животными (лесовод, полевод, цветовод, овощевод, птицевод, животновод, садовод, пчеловод);&nbsp;</p><p>• проводить профилактику заболеваний растений и животных (ветеринар, врач карантинной службы).&nbsp;</p><p>Психологические требования профессий «человек-природа»:</p><p>• развитое воображение, наглядно-образное мышление, хорошая зрительная память, наблюдательность, способность предвидеть и оценивать изменчивые природные факторы;&nbsp;</p><p>• поскольку результаты деятельности выявляются по прошествии довольно длительного времени, специалист должен обладать терпением, настойчивостью, должен быть готовым работать вне коллективов, иногда в трудных погодных условиях, в грязи и т. п.&nbsp;</p>',
    image: require('./images/result/prir.png'),
  },
  {
    name: 'Человек-техника',
    detail:
      '<p>Если вам нравятся лабораторные работы по физике, химии, электротехнике, если вы делаете модели, разбираетесь в бытовой технике, если вы хотите создавать, эксплуатировать или ремонтировать машины, механизмы, аппараты, станки, то ознакомьтесь с профессиями «человек-техника».&nbsp;</p><p>Предметом труда для представителей большинства профессий типа «человек техника» являются:</p><p>• технические объекты (машины, механизмы);&nbsp;</p><p>• материалы, виды энергии.&nbsp;</p><p>Специалистам в этой области приходится выполнять следующие виды деятельности:&nbsp;</p><p>• создание, монтаж, сборка технических устройств (специалисты проектируют, конструируют технические системы, устройства, разрабатывают процессы их изготовления. Из отдельных узлов, деталей собирают машины, механизмы, приборы, регулируют и налаживают их);&nbsp;</p><p>• эксплуатация технических устройств (специалисты работают на станках, управляют транспортом, автоматическими системами);&nbsp;</p><p>• ремонт технических устройств (специалисты выявляют, распознают неисправности технических систем, приборов, механизмов, ремонтируют, регулируют, налаживают их).&nbsp;</p><p>Психологические требования профессий «человек-техника»:&nbsp;</p><p>• хорошая координация движений;&nbsp;</p><p>• точное зрительное, слуховое, вибрационное и кинестетическое восприятие;&nbsp;</p><p>• развитое техническое и творческое мышление и воображение;&nbsp;</p><p>• умение переключать и концентрировать внимание;&nbsp;</p><p>• наблюдательность.&nbsp;</p>',
    image: require('./images/result/tekhn.png'),
  },
  {
    name: 'Человек-человек',
    detail:
      '<p>Предметом труда для представителей большинства профессий типа «человек человек» являются:</p><p><br></p><p>• люди.&nbsp;</p><p>Специалистам в этой области приходится выполнять следующие виды деятельности:</p><p>• воспитание, обучение людей (воспитатель, учитель, спортивный тренер);&nbsp;</p><p>• медицинское обслуживание (врач, фельдшер, медсестра, няня);&nbsp;</p><p>• бытовое обслуживание (продавец, парикмахер, официант, вахтер);&nbsp;</p><p>• информационное обслуживание (библиотекарь, экскурсовод, лектор);&nbsp;</p><p>• защита общества и государства (юрист, милиционер, инспектор, военнослужащий).&nbsp;</p><p>Психологические требования профессий «человек-человек»:&nbsp;</p><p>• стремление к общению, умение легко вступать в контакт с незнакомыми людьми;&nbsp;</p><p>• устойчивое хорошее самочувствие при работе с людьми;&nbsp;</p><p>• доброжелательность, отзывчивость;&nbsp;</p><p>• выдержка;&nbsp;</p><p>• умение сдерживать эмоции;&nbsp;</p><p>• способность анализировать поведение окружающих и свое собственное, понимать намерения и настроение других людей, способность разбираться во взаимоотношениях людей, умение улаживать разногласия между ними, организовывать их взаимодействие;&nbsp;</p><p>• способность мысленно ставить себя на место другого человека, умение слушать, учитывать мнение другого человека;&nbsp;</p><p>• способность владеть речью, мимикой, жестами;&nbsp;</p><p>• развитая речь, способность находить общий язык с разными людьми;&nbsp;</p><p>• умение убеждать людей;&nbsp;</p><p>• аккуратность, пунктуальность, собранность;&nbsp;</p><p>• знание психологии людей.&nbsp;</p>',
    image: require('./images/result/chelo_chel.png'),
  },
  {
    name: 'Человек-знаковая система',
    detail:
      '<p>Если вы любите выполнять вычисления, чертежи, схемы, вести картотеки, систематизировать различные сведения, если вы хотите заниматься программированием, экономикой или статистикой и т. п., то знакомьтесь с профессиями типа «человек -знаковая система». Большинство профессий этого типа связано с переработкой информации.</p><p>Предметом труда для представителей большинства профессий типа «человек знаковая система» являются:&nbsp;</p><p>• тексты на родном или иностранном языках (редактор, корректор, машинистка, делопроизводитель, телеграфист, наборщик);&nbsp;</p><p>• цифры, формулы, таблицы (программист, оператор ЗВМ, экономист, бухгалтер, статистик);&nbsp;</p><p>• чертежи, схемы, карты (конструктор, инженер-технолог, чертежник, копировальщик, штурман, геодезист);&nbsp;</p><p>• звуковые сигналы (радист, стенографист, телефонист, звукооператор).&nbsp;</p><p>Психологические требования профессий «человек-знаковая система»:&nbsp;</p><p>• хорошая оперативная и механическая память;&nbsp;</p><p>• способность к длительной концентрации внимания на отвлеченном (знаковом) материале;&nbsp;</p><p>• хорошее распределение и переключение внимания;&nbsp;</p><p>• точность восприятия, умение видеть то, что стоит за условными знаками;&nbsp;</p><p>• усидчивость, терпение;&nbsp;</p><p>• логическое мышление.&nbsp;</p>',
    image: require('./images/result/znak.png'),
  },
  {
    name: 'Человек-художественный образ',
    detail:
      '<p>Предметом труда для представителей большинства профессий типа «человек знаковая система» является:</p><p>• художественный образ, способы его построения.&nbsp;</p><p>Специалистам в этой области приходится выполнять следующие виды деятельности:</p><p>• создание, проектирование художественных произведений (писатель, художник, композитор, модельер, архитектор, скульптор, журналист, хореограф);&nbsp;</p><p>• воспроизведение, изготовление различных изделий по образцу (ювелир, реставратор, гравер, музыкант, актер, столяр-краснодеревщик);&nbsp;</p><p>• размножение художественных произведений в массовом производстве (мастер по росписи фарфора, шлифовщик по камню и хрусталю, маляр, печатник).&nbsp;</p><p>Психологические требования профессий «человек-художественный образ»:&nbsp;</p><p>• художественные способности; развитое зрительное восприятие;&nbsp;</p><p>• наблюдательность, зрительная память; наглядно-образное мышление; творческое воображение;&nbsp;</p><p>• знание психологических законов эмоционального воздействия на людей.&nbsp;</p>',
    image: require('./images/result/khud.png'),
  },
];


var ar_result_en = [
  {
    name: 'Person to nature',
    detail:
      "<p>This personality type is drawn to careers that address and/or involve elements of our natural environment: from ecology to wildlife, animal care to conservation. The following subjects will help you get a grasp of these career types: biology, anatomy, physiology, zoology and botany.</p><p>The types of activities specific to this career type are sophisticated, volatile, and unconventional. Plants, animals and microorganisms are constantly developing, growing, changing as well as getting sick and dying in some cases. The professionals in this area would need not just to have a strong working knowledge of the relevant subject matter, but would also need to have a natural motivation for understanding and forecasting how contextual and conditional change in the field will take place over time.</p><p>In addition to addressing the elements of environment, the jobs for this career personality type often involve work with various sophisticated equipment, data collection and in some cases budgeting. Some of the careers specific for this personality type might also require artistic or creative skills, for example, landscapers and landscape architects. &nbsp;</p><p>Even though the areas of microbiology, horticulture and animal husbandry have become computerized and some processes can be automated by equipment, the technology is just a tool that needs to be managed by people. The key for anyone working in these fields is a curiosity about the environment and its elements.&nbsp;</p><p>Many people like being outdoors, hiking or spending time in nature. However, it's not the same kind of curiosity about the environment that people of this career personality type have. Hence it's important to figure out what kind of curiosity about the nature you have when choosing an occupation. Is it more that you like nature the nature as the environment in which you can create things and observe them? Or is it more of a nice place for you for hiking, camping, etc. You can get a better understanding of which one is true for you while doing the practical exercises.</p><p>Some examples of occupations for this career personality type:</p><p>Ecologist; Agronomist;<br>Geneticist;<br>Farmer;<br>Breeder Agricultural and Food Scientists;<br>Horticulturist;<br>Soil and Plant Scientist;<br>Conservation Scientist;<br>Geologist;<br>Zoologist/Wildlife Biologist;<br>Botanist;<br>Park Ranger;<br>Landscaper or Landscape Architect;<br>Veterinarian;<br>Arborist;<br>Microbiologist; &nbsp; &nbsp; &nbsp; &nbsp;</p><p>New careers that were created recently or will be created in nearest future: Experts also estimate that in a few years there will be new careers created that will be addressing biology and environment like bioethicist, IT geneticist, clinical bioinformatician and urban ecologist. &nbsp;</p>",
    image: require('./images/result/prir.png'),
  },
  {
    name: 'Person to machine',
    detail: "<p>This career personality type is drawn to careers that address and involve working with technical objects: cars, general equipment, tools, technological operations, textiles or other materials. The following subjects will help you get a grasp of these career types: physics, chemistry, mathematics, technical drawing, computer classes, and programming. The types of careers for this personality type also include programming, IT development, engineering and work with non-technical objects such as fabrics, food, plastic, metal, wood or other materials. These types of objects are static and often can be measured or defined by many characteristics. When working with these objects, the professionals need to have accuracy and precision. &nbsp;</p><p>There are lot of opportunities for innovations in this field. You can create and design not only new products, but also new technologies and software that can change how people work, live, or do certain things. &nbsp;</p><p>Additionally, these types of careers require a discipline.</p><p>This career personality type offers the biggest variety of occupations.&nbsp;</p><p><strong>Some examples of occupations for this career personality type:</strong></p><p>Mechanical Engineer;<br>Chemical Engineer;<br>Electrical and Electronics Engineering Technicians;<br>Architect;<br>Electrician;<br>Driver;<br>Pilot;<br>Chef;<br>Baker;<br>Food Engineer;<br>Construction Equipment Operator;<br>Construction Worker;<br>Solar Photovoltaic Installers;<br>System Administrator;<br>Mechanic;<br>Aerospace Engineering and Operations Technicians and many others.</p><p><strong>New careers that were created recently or will be created in the nearest future:</strong> Smart Home Technology Project Engineer and Automation Engineer, Fabric Engineer, Industrial Equipment Maintenance Supervisor, Environmental engineer, Recycling officer, Sustainability consultant and Sustainable Design Architects, Renewable Energy Engineers.&nbsp;</p>",
    image: require('./images/result/tekhn.png'),
  },
  {
    name: 'Person to person',
    detail: "<p>This career personality type includes professions that address and directly involve working with others. Some examples of the careers specific for this type are in education, customer service, and management. The following subjects will help you get a grasp of these career types: history, literature, languages, social science, psychology, sociology and others subjects related to exploring human nature and society. &nbsp;&nbsp;</p><p>The jobs specific for this CPT primarily involve interaction and communication with other people. Another determining characteristic of this CPT is that you are comfortable with and able to achieve the following two skills: 1. Knowing how to build and support relationships with others, understanding them and their needs, and knowing how to work with them; 2. Have the requisite knowledge and educational attainment specific to the field or area you work in, such as: technology, education, art, manufacturing, science, etc.&nbsp;</p><p>Some examples of occupations for this career personality type:&nbsp;</p>&nbsp;Project Manager;<br>&nbsp;Chief Executive Officer;<br>&nbsp;Teacher;<br>&nbsp;Trainer;<br>&nbsp;HR Manager;<br>&nbsp;Public Relations Manager;<br>&nbsp;Advertising Manager;<br>&nbsp;Sales Person;<br>&nbsp;Waiter;<br>&nbsp;Hair Stylist;<br>&nbsp;Doctor;<br>&nbsp;Art Director;<br>&nbsp;Nurse;<br>&nbsp;Attorney;<br>&nbsp;Events Manager;<br><p>New careers that were created recently or will be created in the nearest future: Cognitive Retraining Specialist, Community Development Managers, Child Protection Specialist, Education Counselor, Social Media Advertising Manager, Personal Branding Consultant, UI/UX Architect.</p><p></p>",
    image: require('./images/result/chelo_chel.png'),
  },
  {
    name: 'Person to symbol',
    detail: "<p>This career personality type includes the professions that are addressing data processing. Data can be represented in numbers, characters, formulas or content.</p><p></p><p>Today, we are all heavily engaged with data: graphs, drawings, maps, numbers, tables, content, street signs and many other signs and characters are constantly surrounding us.&nbsp;</p><p>When working with characters, the same as working with any objects, there is a need to audit the quality, track and analyze data, structure it and itfffs often needed to create new characters or character systems. In other words, there are opportunities to be creative and innovative in both the physical and digital realms.</p><p>Some examples of occupations for this career personality type:</p>&nbsp;IT Programmer;<br>&nbsp;Software Engineer;<br>&nbsp;IT Security Specialist;<br>&nbsp;Accountant;<br>&nbsp;Finance Manager;<br>&nbsp;Interpreter;<br>&nbsp;Editor;<br>&nbsp;Economist;<br>&nbsp;Data Analyst;<br>&nbsp;Engineer;<br>&nbsp;Financial Analyst;<br>&nbsp;Content Creator;<br>&nbsp;Copywriter;<br>&nbsp;Air Traffic Control Operator;<br><p>New careers that were created recently or will be created in the nearest future:</p><p>AI Engineer, Big Data Engineer, Digital Analyst.</p><p></p>",
    image: require('./images/result/znak.png'),
  },
  {
    name: 'Person to art',
    detail: "<p>This career personality type includes all professions pertaining to art and artistry, including:&nbsp;</p><p></p><p>Professions related to creating art, drawing, painting, etc.</p>&nbsp;Professions related to creating music, editing it, playing musical instruments, singing, recording music, etc. &nbsp;<p>Professions related to writing books, storytelling, writing stories for movies, etc.</p><p>Professions related to acting, dancing, performing, etc.</p><p>The definitive characteristic of an artistic career is that a significant part of the work is accomplished without the presence of an audience. Moreover, often the purpose of art is to make it seem as authentic as possible. For example, an actor or artist may be performing or showcasing their prepared works to the public or an audience for only a very brief amount of time, relative to how long they worked on it. In order for the actor to make this short performance happen, he or she needs to rehearse for many hours daily, constantly work on his or her skills, be very disciplined and follow the schedule and routines.&nbsp;</p><p>When choosing a career in art, it is important to consider this aspect that might not be obvious at first. Besides that, it is important to release yourself from the opinions of others in order to enjoy the work you will be doing as some people might not like or not understand your art. &nbsp;</p><p>Some examples of occupations for this career personality type:</p><p>Actor;<br>&nbsp;Fashion Designer;<br>&nbsp;Graphic Designer;<br>&nbsp;UX/UI Designer;<br>&nbsp;Photographer;<br>&nbsp;Artist;<br>&nbsp;Painter;<br>&nbsp;Artistic Director;<br>&nbsp;Jeweler;<br>&nbsp;Makeup artist;<br>&nbsp;Choreographer;<br>&nbsp;Musician;<br>&nbsp;Dancer;<br>&nbsp;Writer;<br>&nbsp;Journalist;<br>&nbsp;Singer; etc.&nbsp;</p><p>New careers that were created recently or will be created in the nearest future: Virtual Reality designer, Augmented Reality designer, Avatar programmer, Cybernetic director.</p><p></p>",
    image: require('./images/result/khud.png'),
  },
];
