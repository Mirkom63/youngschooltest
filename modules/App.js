import {YellowBox} from 'react-native';
YellowBox.ignoreWarnings(['Remote debugger']);

import React from 'react';
import {
  View,
  Text,
  Button,
  Image,
  Dimensions,
  ImageBackground,
  SafeAreaView,
  TouchableOpacity,
  Share,
  ScrollView,
  ActivityIndicator,
  FlatList,
  RefreshControl,
} from 'react-native';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import HTMLView from 'react-native-htmlview';
import moment from "moment";
import AsyncStorage from '@react-native-community/async-storage';


import {AccessToken, LoginManager} from 'react-native-fbsdk';
import VKLogin from 'react-native-vkontakte-login';

const axios = require('axios');
var host="https://api.youngschool.ru";


import appleAuth, {
  AppleButton,
  AppleAuthRequestOperation,
  AppleAuthRequestScope,
  AppleAuthCredentialState,
} from '@invertase/react-native-apple-authentication';
