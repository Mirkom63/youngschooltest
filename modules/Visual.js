
var lang_layout={
  'en' : {
    'home' : "Home",
  },
  'ru' : {
    'home' : "Главная",
  }
}


//layout
const HomeStack = createStackNavigator({
  Home: Home,
});

const TestStack = createStackNavigator({
  Home: TestDetail,
});

const ListStack = createStackNavigator({
  List: List,
  Результат: ListDetail,
});

const  TabNavigatorEn = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
    },
    Test: {
      screen: TestStack,
    },
    Results: {
      screen: ListStack,
    },
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) =>
        getTabBarIconEn(navigation, focused, tintColor),
    }),
    tabBarOptions: {
      activeTintColor: '#307def',
      inactiveTintColor: 'gray',
    },
  },
);

var getTabBarIconEn = (navigation, focused, tintColor) => {
  switch (navigation.state.key){
    case 'Home':
      return (
        <View>
          <View style={{width: 25, height: 25}}>
            {focused ? (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/home-active.png')}
              />
            ) : (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/home.png')}
              />
            )}
          </View>
        </View>
      );
    case 'Test':
      return (
        <View>
          <View style={{width: 25, height: 25}}>
            {focused ? (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/test-active.png')}
              />
            ) : (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/test.png')}
              />
            )}
          </View>
        </View>
      );
    case 'Results':
      return (
        <View>
          <View style={{width: 25, height: 25}}>
            {focused ? (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/list-active.png')}
              />
            ) : (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/list.png')}
              />
            )}
          </View>
        </View>
      );
  }
};


const TabNavigatorRu = createBottomTabNavigator(
  {
    Главная: {
      screen: HomeStack,
    },
    Тест: {
      screen: TestStack,
    },
    Результаты: {
      screen: ListStack,
    },
  },
  {
    initialRouteName: 'Главная',
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) =>
        getTabBarIconRu(navigation, focused, tintColor),
    }),
    tabBarOptions: {
      activeTintColor: '#307def',
      inactiveTintColor: 'gray',
    },
  },
);


var getTabBarIconRu = (navigation, focused, tintColor) => {
  switch (navigation.state.key){
    case 'Главная':
      return (
        <View>
          <View style={{width: 25, height: 25}}>
            {focused ? (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/home-active.png')}
              />
            ) : (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/home.png')}
              />
            )}
          </View>
        </View>
      );
    case 'Тест':
      return (
        <View>
          <View style={{width: 25, height: 25}}>
            {focused ? (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/test-active.png')}
              />
            ) : (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/test.png')}
              />
            )}
          </View>
        </View>
      );
    case 'Результаты':
      return (
        <View>
          <View style={{width: 25, height: 25}}>
            {focused ? (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/list-active.png')}
              />
            ) : (
              <Image
                style={{width: 25, height: 25}}
                source={require('./images/list.png')}
              />
            )}
          </View>
        </View>
      );
  }
};




var root_app;
class RootApp extends React.Component {

  constructor(props) {
    super(props);
    root_app = this;

    this.state = {
      auth: false,
      user_id: false,
      load_page: false,
      language: null,
    };


  }

  componentDidMount(){
    root_app=this;

    AsyncStorage.getItem('user_id').then(function(user_id){
      if(user_id==undefined){
        root_app.setState({
          auth: false,
          user_id: false,
        });
      }else{
        root_app.setState({
          auth: true,
          user_id: user_id,
        });
      }

    });


    AsyncStorage.getItem('language').then(function(language){

      if(language=='en'){
        data_test=data_test_en;
        ar_result=ar_result_en;
      }
      if(language=='ru'){
        data_test=data_test_ru;
        ar_result=ar_result_ru;
      }

      root_app.setState({
        language: language,
      });
    });


  }

  onShare(result_number) {
    if(root_app.state.language=='en'){
      Share.share({
        message: 'The best career field for me is '+ar_result[result_number]['name'].replace('Person to ','')+' http://api.onlineyoung.com/?share='+result_number,
      });
    }
    if(root_app.state.language=='ru'){
      Share.share({
        message: 'Я - '+ar_result[result_number]['name']+' https://api.youngschool.ru/?share='+result_number,
      });
    }

  }

  authorization(type,user_id,email){

    axios.get(host+'/create_user.php',{params: {platform: Platform.OS,language: 'ru', type: type, user_id: user_id, email: email } }).then(function (response) {

      AsyncStorage.setItem('user_id',response.data.toString()).then(function(){});


      root_app.setState({
        auth: true,
        user_id: response.data,
      },function(){

        root_app.setState({
          load_page: false,
        });

        if(test_root_app!=undefined){
          if(test_root_app.state.page==21){
            test_root_app.setState({
              finish: true,
            });
            test_root_app.save_result();

          }

        }

        if(list_root_app!=undefined){
          list_root_app.props.navigation.setParams({ auth: false });
          list_root_app.updateResult();
        }


      });
    });


  }


  onAppleButtonPress = async () => {

    //
    // // performs login request
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: AppleAuthRequestOperation.LOGIN,
      requestedScopes: [AppleAuthRequestScope.EMAIL, AppleAuthRequestScope.FULL_NAME],
    });
    //
    //
    // get current authentication state for user
    // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
    const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);

    // use credentialState response to ensure the user is authenticated
    if (credentialState === AppleAuthCredentialState.AUTHORIZED) {

      root_app.authorization('apple',appleAuthRequestResponse.user,appleAuthRequestResponse.email);

    }

  }


  handleFBLogin = async () => {
    try {
      LoginManager.logOut();
      if (Platform.OS === 'android') {
        LoginManager.setLoginBehavior('web_only');
      }
      const result = await LoginManager.logInWithPermissions([
        'public_profile',
      ]);
      if (!result.error && !result.isCancelled) {
        const data = await AccessToken.getCurrentAccessToken();
        const token = data?.accessToken || '';

        axios.post('https://graph.facebook.com/me?',{fields: 'id,email',oauth_token: token}).then(function (response) {
          root_app.authorization('fb',response.data.id,response.data.email);
  	  	});
      }

      AsyncStorage.removeItem('user_id').then(function(){});

    } catch (error) {
      console.log('error', error);
    }
  };

  handleVKLogin = async () => {
    try {
      await VKLogin.logout();
      const auth = await VKLogin.login(['email']);
      root_app.authorization('vk',auth.user_id,auth.email);

    } catch (error) {
      console.log('error', error);
    }
  };


  set_language(new_language){

    //language=new_language;

    if(new_language=='en'){
      data_test=data_test_en;
      ar_result=ar_result_en;
    }
    if(new_language=='ru'){
      data_test=data_test_ru;
      ar_result=ar_result_ru;
    }

    AsyncStorage.setItem('language',new_language).then(function(){
      root_app.setState({
        language: new_language,
      });
    });




  }


  render(){
    if(this.state.language==undefined){
      return(
        <Language/>
      );
    }else{
      if(this.state.load_page==true){
        return(
          <ActivityIndicator style={{flex: 1}} size="large" color="#aaa" />
        );
      }else{
        if(this.state.language=='ru'){
          var AppContainer = createAppContainer(TabNavigatorRu);
        }else{
          var AppContainer = createAppContainer(TabNavigatorEn);
        }

        return(
          <AppContainer/>
        );
      }
    }
  }
}

export default function App() {
  return <RootApp/>;
}
