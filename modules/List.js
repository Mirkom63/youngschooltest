
var lang_list={
  'en' : {
    'results' : "Results",
    'result' : "Result",
    'log_out' : 'Log out',
    'auth_info' : 'To get the results sign in with:',
    'button': 'Begin test',
    'no_test' : "You don't have any test results",
    'title' : "Begin test and learn what career is right for you!",
    'page' : 'Test',
  },
  'ru' : {
    'results' : "Результаты",
    'result' : "Результат",
    'log_out' : 'Выйти',
    'auth_info' : 'Чтобы посмотреть свои результаты, авторизуйтесь:',
    'button' : 'Пройти тест',
    'no_test' : 'У вас нет результатов тестирования',
    'title' : "Пройди тест и узнай кем ты можешь стать!",
    'page' : 'Тест',
  }
}

function ResultItem(props){
  return(
    <TouchableOpacity
        onPress={props.onSelect}
        style={{borderBottomWidth: 1, borderBottomColor: '#ddd',padding: 10, paddingTop: 20, paddingBottom: 20, flexDirection: 'row', flex: 1, justifyContent: 'space-between'}}>
        <Text  style={{fontSize: 13}}>{ar_result[props.answer_id]['name']}</Text>
        <Text style={{color: '#407de7', marginLeft: 10, marginTop: 0}}>{moment(props.created_at).format('DD.MM.YY HH:mm')}</Text>
    </TouchableOpacity>
  )
}

var list_root_app;

class List extends React.Component {

  static navigationOptions = ({ navigation }) => {

    const { params } = navigation.state;

    return {
      title: lang_list[root_app.state.language]['results'],
      headerRight: () => (
        root_app.state.auth==true && <Button onPress={()=>list_root_app.logOut()} title={lang_list[root_app.state.language]['log_out']}/>
      ),
    };

  };


  componentDidMount(){

    list_root_app=this;
    this.props.navigation.setParams({ auth: false });

    this.updateResult()
  }


  state={
    results: [],
  }

  logOut(){
    root_app.setState({
      auth: false,
      user_id: null,
    });
    this.setState({
      results: [],
    });
    this.props.navigation.setParams({ auth: false });
    if(test_root_app!=undefined){
      test_root_app.restartTest();
    }
    AsyncStorage.removeItem('user_id').then(function(){});

  }

  updateResult(){

    list_root_app.setState({
      load_page: true,
    },function(){
      setTimeout(function(){
        axios.get(host+'/get_result.php',{params:
          {
            user_id: root_app.state.user_id,
          }
        }).then(function (response) {

          list_root_app.setState({
            load_page: false,
            results: response.data
          });

        });
      },300);

    });



  }

  openResult(answer_id){
    this.props.navigation.navigate('Результат',{
      result_number: answer_id,
    })
  }



  render(){



    if(root_app.state.auth==false){
      return(
        <View style={{padding: 30, flex: 1, flexDirection: 'column', justifyContent: 'center'}}>

          <View style={{flex: 1,justifyContent: 'center', alignItems: 'center',}}>

            <Text style={{textAlign: 'center', marginBottom: 30, fontSize: 20}}>{lang_list[root_app.state.language]['auth_info']}</Text>

            { root_app.state.language=='ru' &&
              <TouchableOpacity onPress={root_app.handleVKLogin}>
                <Image
                  style={{width: 300, height: 57}}
                  source={require('./images/vk_ru.png')}
                />
              </TouchableOpacity>
            }
            <TouchableOpacity onPress={root_app.handleFBLogin}>
              { root_app.state.language=='ru' &&
                <Image
                  style={{width: 300, height: 57, marginTop: 30}}
                  source={require('./images/fb_ru.png')}
                />
              }
              { root_app.state.language=='en' &&
                <Image
                  style={{width: 300, height: 57, marginTop: 30}}
                  source={require('./images/fb_en.png')}
                />
              }

            </TouchableOpacity>

            <AppleButton
              buttonStyle={AppleButton.Style.BLACK}
              buttonType={AppleButton.Type.SIGN_IN}
              style={{width: 300, height: 57, marginTop: 30}}
              onPress={() => root_app.onAppleButtonPress()}
            />


          </View>


        </View>
      );
    }else{
      if(this.state.results.length==0){
        return (

          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center',padding: 30}}>

            <Text style={{
              fontSize: 22,
              textAlign: 'center',
            }}>{lang_list[root_app.state.language]['no_test']}</Text>

            <Text
              style={{
                fontSize: 16,
                textAlign: 'center',
                marginTop: 30,
                marginBottom: 30,
              }}>
              {lang_list[root_app.state.language]['title']}
            </Text>

            <TouchableOpacity style={{width: 200, height: 40, backgroundColor: '#407de7', borderRadius: 5}} onPress={()=>this.props.navigation.navigate(lang_list[root_app.state.language]['page'])}>
              <Text style={{color: '#FFF', textAlign: 'center', lineHeight: 40, fontSize: 20}}>{lang_list[root_app.state.language]['button']}</Text>
            </TouchableOpacity>




          </View>
        );
      }else{
        return (
            <ScrollView
              refreshControl={
                <RefreshControl refreshing={false} onRefresh={()=>list_root_app.updateResult()} />
              }
              >
              <FlatList
                data={this.state.results}
                renderItem={({item}) =>
                  <ResultItem
                      answer_id={item.answer_id}
                      created_at={item.created_at}
                      onSelect={()=>this.openResult(item.answer_id)}
                  />
                }
                keyExtractor={item => item.id}
              />
            </ScrollView>
        );
      }

    }


  }
}
