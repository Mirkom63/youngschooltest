
class ListDetail extends React.Component {

  static navigationOptions = ({ navigation }) => {

    if(root_app.state.language=='ru'){
      return {
        title: "",
        headerRight: () => <Button onPress={()=>root_app.onShare(navigation.getParam('result_number'))} title="Поделиться"/>
      };
    }

    if(root_app.state.language=='en'){
      return {
        title: "",
        headerRight: () => <Button onPress={()=>root_app.onShare(navigation.getParam('result_number'))} title="Share"/>
      };
    }

  };

  render(){

    var width = Dimensions.get('window').width;

    return (
      <SafeAreaView style={{backgroundColor: '#FFF', flex: 1}}>
        <ScrollView>
          <View style={{backgroundColor: '#FFF'}}>
            <View>
              <Text
                style={{fontSize: 30, textAlign: 'center', padding: 30}}>
                {ar_result[this.props.navigation.getParam('result_number')]['name']}
              </Text>
              <Image
                source={ar_result[this.props.navigation.getParam('result_number')]['image']}
                style={{width: '100%', height: width}}
              />
              <HTMLView
                style={{padding: 30}}
                value={ar_result[1].detail}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
