var gulp = require('gulp');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();

gulp.task('js', function() {
  return gulp.src('./modules/*.js')
    .pipe(concat('App.js'))
    .pipe(gulp.dest('./'));
});
